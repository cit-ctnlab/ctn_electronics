This is experiment in the design of a low noise preamp with sub 1nV/sqrtHz noise density.  The design concept is to gang up a bunch of parraallel low noise LT1128 op amp circuits into a summing junction.  Parrallel amplification into a summing junction leads to an incoherent sum of noise but coherent sum of signal.  The result is a 1/sqrt(N) improment in input refered noise.

It is likely that reduction of voltage noise comes at the cost of current noise.  This means that such an amplifier is optimal for less stiff, i.e. low impedance, sources.  High impedance sources are better off with a low noise FET input amplifier. For sources with a capacitive load, like AC coupling (12mHz, wt R=200k, C=66uF), current noise will, dominate at low frequency but have higher performance at higher frequencies.

It is all application dependant.

