This folder contains models and experiments in post mixer RF filters for dumping omega and 2omega terms produced by the mixer.  The goal is to create a LP filter that rejects all these higher frequency mixer terms without reflecting them back into the mixer.

The current LP filter is a Cauer filter (three branch elliptic filter) that is terminated with 22 ohm on one side and 124 ohm on the other.  It is not clear if the RF is being properly dumpped in the stop band and if the impedance has been properly set on input and output.  

Original values were for 21.5 MHz filter with a notch at that freqeuency.  It turns out these were never modified to the actual operating frequencies of the PDH modulation.