# RFPDzeroCTN

### Zero representation of LIGO RFPD D980454-00-C in CTN lab

This module is created so that all changes to the RFPDs in CTN can be put in
one place and passed onto the circuit definition when being used. This will
ensure that all future uses of the zero model, defined in the RFPD package are
same and have all changes incorporated.

## Usage
Put RFPDzeroCTN in your python path and in any script or notebook, do:
```
from RFPDzeroCTN import *
```
After this, you'll get objects named SN009 and SN010 which are circuit
instances of RFPDs of same serial number in CTN.
## Email
Anchal Gupta - anchal@caltech.edu
