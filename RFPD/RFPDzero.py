
# coding: utf-8

# In[1]:


from zero import Circuit
from zero.components import Resistor, Capacitor, Inductor
import numpy as np


# ### Helper Function

# In[2]:


def createNLCompFunc(name,node1,node2,comType):
    if comType is "capacitor":
        def NLcomp(circuit,present=False, value="0"):
            if present:
                if circuit.has_component(name):
                    circuit.remove_component(name)
                circuit.add_capacitor(name=name,value=value, node1=node1, node2=node2)
            elif circuit.has_component(name):
                circuit.remove_component(name)
    elif comType is "resistor":
        def NLcomp(circuit,present=False, value="0"):
            if present:
                if circuit.has_component(name):
                    circuit.remove_component(name)
                circuit.add_resistor(name=name,value=value, node1=node1, node2=node2)
            elif circuit.has_component(name):
                circuit.remove_component(name)
    elif comType is "inductor":
        def NLcomp(circuit,present=False, value="0"):
            if present:
                if circuit.has_component(name):
                    circuit.remove_component(name)
                circuit.add_inductor(name=name,value=value, node1=node1, node2=node2)
            elif circuit.has_component(name):
                circuit.remove_component(name)
    else:
        print("Error:")
        print("Use comType capacitor , resistor or inductor")
    return NLcomp

# Function to list all components connected to some particular node
from zero.components import Node
def cmpConnTo(nodeName):
    nx = Node(nodeName)
    for cmp in circuit.components:
        if nx in cmp.nodes:
            print(cmp)


# ***
# ***
# # Circuit Definition
# Following are some useful node/component names to remember:
# 
# |	Node name	|	Function/Parameter	|
# |	---	|	---	|
# |<img width=100/>| <img width=600/> |
# |	nTestIn	|	Test Input Port	|
# |	nPDIn	|	Current Input for PD	|
# |	nRFOut	|	RF Output	|
# |   nDCOut  |   DC Output   |
# |   Cpd     |   PD Capacitance   |
# 
# Nomenclature:
# * All nodes are named as 'n' + number. Special node names which are defined for easy access are listed above.
# * All circuit elements are names as defined in schematic. eg. C14.
# 
# ### Initialize circuit object

# In[3]:


LIGO_D980454_00_C = Circuit()


# ***
# ## [RFPD Board LIGO-D980454-00-C](https://dcc.ligo.org/DocDB/0020/D980454/001/D980454-00.pdf)
# ### Test Input Port
# <img src="images/TestInPort.png" style="max-height:200px; width:auto;" align="left">

# In[4]:


LIGO_D980454_00_C.add_resistor(name="R14", value="50", node1="nTestIn", node2="gnd")
LIGO_D980454_00_C.add_resistor(name="R15", value="100k", node1="nTestIn", node2="nPDIn")


# ### 1-Omega Band Pass Filter
# <img src="images/1OmegaBandPass.png" style="max-height:200px; width:auto;" align="left">

# In[5]:


LIGO_D980454_00_C.add_capacitor(name="Cpd",value="150p", node1="nPDIn", node2="gnd")
LIGO_D980454_00_C.add_resistor(name="Rpdsh", value="25e6", node1="nPDIn", node2="gnd")
LIGO_D980454_00_C.add_inductor(name="L2", value="81n", node1="nPDIn", node2="n1")
LIGO_D980454_00_C.add_inductor(name="L1", value="142n", node1="n1", node2="n2")
LIGO_D980454_00_C.add_capacitor(name='C4', value="0.0056u", node1="n2", node2="gnd")


# ### DC Transimpedance Amplifier
# <img src="images/DCTIA.png" style="max-height:200px; width:auto;" align="left">

# In[6]:


LIGO_D980454_00_C.add_resistor(name="R4", value="750", node1="n2", node2="gnd")
LIGO_D980454_00_C.add_resistor(name="R5", value="750", node1="n2", node2="gnd")
LIGO_D980454_00_C.add_library_opamp(name="U5", model="op27", node1="n2", node2="n3", node3="n3")


# ### DC 2nd Stage Amplifier
# <img src="images/DC2Amp.png" style="max-height:300px; width:auto;" align="left">

# In[7]:


LIGO_D980454_00_C.add_library_opamp(name="U6", model="op27", node1="n3", node2="n4", node3="n5")
LIGO_D980454_00_C.add_resistor(name="R11", value="1k", node1="n4", node2="gnd")
LIGO_D980454_00_C.add_resistor(name="R16", value="909", node1="n4", node2="n5")


# ### DC Out
# <img src="images/DCOut.png" style="max-height:150px; width:auto;" align="left">

# In[8]:


LIGO_D980454_00_C.add_resistor(name="R7", value="20", node1="n5", node2="nDCOut")


# ### 2-Omega Band Stop Filter
# <img src="images/2OmegaBandStop.png" style="max-height:400px; width:auto;" align="left">

# In[9]:


LIGO_D980454_00_C.add_capacitor(name="C39",value="5.6p", node1="nPDIn", node2="n6")
LIGO_D980454_00_C.add_capacitor(name="C40",value="2p", node1="nPDIn", node2="n6")
LIGO_D980454_00_C.add_inductor(name="L8", value="320n", node1="n6", node2="gnd")

# C14, C1 and L3 are typically not populated.
# LIGO_D980454_00_C.add_capacitor(name="C14",value="5.6p", node1="nPDIn", node2="n7")
# LIGO_D980454_00_C.add_capacitor(name="C1",value="2p", node1="nPDIn", node2="n7")
# LIGO_D980454_00_C.add_inductor(name="L3", value="320n", node1="nPDIn", node2="n7")
LIGO_D980454_00_C.add_resistor(name="L3Short", value="0", node1="nPDIn", node2="n7")

# C6, C13 and L4 are typically not populated.
# LIGO_D980454_00_C.add_capacitor(name="C6",value="5.6p", node1="n7", node2="n8")
# LIGO_D980454_00_C.add_capacitor(name="C13",value="2p", node1="n7", node2="n8")
# LIGO_D980454_00_C.add_inductor(name="L4", value="320n", node1="n8", node2="gnd")


# ### RF Amplifier
# <img src="images/RFAmp.png" style="max-height:300px; width:auto;" align="left">

# In[10]:


LIGO_D980454_00_C.add_capacitor(name="C10",value="0.1u", node1="n7", node2="n9")
LIGO_D980454_00_C.add_resistor(name="R3", value="1k", node1="n9", node2="gnd")
LIGO_D980454_00_C.add_inductor(name="L7", value="1.45u", node1="n9", node2="gnd")
LIGO_D980454_00_C.add_library_opamp(name="U1", model="max4107", node1="n9", node2="n10", node3="n11")
LIGO_D980454_00_C.add_resistor(name="R1", value="681", node1="n10", node2="n11")
LIGO_D980454_00_C.add_resistor(name="R2", value="75", node1="n10", node2="gnd")
LIGO_D980454_00_C.add_resistor(name="R6", value="20", node1="n11", node2="nRFOut")


# # Create a script from this notebook
# The cell below this will not be present in script to avoid unnecessary overwrites when running the script.

