
# coding: utf-8

# ***
# ***
# ***
# # Circuit Definition
# Following are some useful node/component names to remember:
# 
# |	Node name	|	Function/Parameter	|
# |	---	|	---	|
# |<img width=100/>| <img width=600/> |
# |	nTestIn	|	Test Input Port	|
# |	nPDIn	|	Current Input for PD	|
# |	nRFOut	|	RF Output	|
# |   nDCOut  |   DC Output   |
# |   Cpd     |   PD Capacitance   |
# 
# Nomenclature:
# * All nodes are named as 'n' + number. Special node names which are defined for easy access are listed above.
# * All circuit elements are names as defined in schematic. eg. C14.
# 
# ## Importing the circuit definition

# In[1]:


from RFPDzero import *
import copy
import numpy as np

# Creating two deepcopies for the North and South Boxes
SN009 = copy.deepcopy(LIGO_D980454_00_C)
SN010 = copy.deepcopy(LIGO_D980454_00_C)


# ***
# ## Adding changes to the circuit in CTN:
# ### Already present differences from schematic:

# In[2]:


# SN009 North
SN009['R14'].value = 49.9
SN009.remove_component('R4')
SN009['R5'].value = 20
SN009['R16'].value = 1000
SN009.remove_component('R3')

# SN010 South
SN010.remove_component('R5')
SN010['R4'].value = 22
SN010['R16'].value = 1000
SN010.remove_component('R3')


# ### These changes follow this [change log](https://nodus.ligo.caltech.edu:30889/ATFWiki/doku.php?id=main:experiments:psl:rfpd).
# ***
# ### [CTN:2338 Changes](https://nodus.ligo.caltech.edu:8081/CTN/2338) on SN009(North) and SN010(South)

# In[3]:


SN009['R6'].value = 50
SN010['R6'].value = 50
SN010['R1'].value = 680
SN010['R2'].value = 75


# ***
# ### [CTN:2346 Changes](https://nodus.ligo.caltech.edu:8081/CTN/2346) on SN010(South)

# In[4]:


SN010['R11'].value = 22


# ***
# ### [CTN:2348 Changes](https://nodus.ligo.caltech.edu:8081/CTN/2348) on SN009(North)

# In[5]:


SN009['R11'].value = 22


# ***
# ## Adding custom tuned values in CTN:
# ### These values are taken from the [Present Known Characteristics Here](https://nodus.ligo.caltech.edu:30889/ATFWiki/doku.php?id=main:experiments:psl:rfpd) and tuning details mentioned in [CTN:2240](https://nodus.ligo.caltech.edu:8081/CTN/2240).
# ***
# #### SN009 (North)

# In[6]:


# 1-Omega Band Pass Filter
SN009fres = 36.036e6  # 36.036 MHz resonant Frequency
SN009['Cpd'].value = 150e-12 # From Datasheet of C30642
SN009.replace_component('L2', Resistor(name='L2short', value='0')) # Short L2
SN009['L1'].value = 1/(SN009['Cpd'].value*(2*np.pi*SN009fres)**2)
print(SN009['L1'])

# 2-Omega Band Stop Filter
SN009fnot = 73.109e6 # 73.109 MHz Notch Frequency
SN009['C39'].value = 5.6e-12
SN009['L8'].value = 320e-9
SN009['C40'].value = 1/(SN009['L8'].value*(2*np.pi*SN009fnot)**2) - SN009['C39'].value
print(SN009['C40'])


# ***
# #### SN010 (South)

# In[7]:


# 1-Omega Band Pass Filter
SN010fres = 36.67e6  # 36.67 MHz resonant Frequency
SN010['Cpd'].value = 150e-12 # From Datasheet of C30642
SN010.replace_component('L2', Resistor(name='L2short', value='0')) # Short L2
SN010['L1'].value = 1/(SN010['Cpd'].value*(2*np.pi*SN010fres)**2)
print(SN010['L1'])

# 2-Omega Band Stop Filter
SN010fnot = 74.237e6 # 74.237 MHz Notch Frequency
SN010['C39'].value = 5.6e-12
SN010['L8'].value = 320e-9
SN010['C40'].value = 1/(SN010['L8'].value*(2*np.pi*SN010fnot)**2) - SN010['C39'].value
print(SN010['C40'])


# # Create a script from this notebook
# The cell below this will not be present in script to avoid unnecessary overwrites when running the script.

