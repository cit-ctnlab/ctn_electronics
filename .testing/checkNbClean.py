"""
Usage: python checkNbClean.py notebook.ipynb

System will exit with code 1 in the event that a notebook in the search path
contains uncleared output. This is so that gitlab CI can catch commits with
bloated content and prompt the user to fix their oversight.

At this stage its not clear how to pass a speific message with the offending
notebook to gitlab CI, but users can find the terminal dialog in the web
interface pipline dialog.

Author: Andrew Wade (awade@ligo.caltech.edu)
Date: Oct 24, 2017

"""

import sys
import io
import os
import fnmatch
from nbformat import read


def main():
    passflag = 1  # flag state one if all notebooks found pass the test
    topdir = sys.argv[1]  # get arg of input dir to search
    for filename in recursive_glob(topdir, "*.ipynb"):  # loop over all files
        if "-checkpoint.ipynb" not in filename:  # exclude checkpoints
            with io.open(filename, 'r', encoding="utf-8") as f:
                # print("Opening file: {}".format(filename))
                nb = read(f, as_version=4)  # open contents of notebook
            if not check_nb_clean(nb):  # run check_nb_clean sub routine
                print("Uncleared cells in notebook located "
                      "at {loc}".format(loc=filename))
                passflag = passflag * 0  # flip flag to failed state
            else:
                print("Notebook clean at {loc}".format(loc=filename))
    if passflag != 1:  # fail when just one notebook has output
        sys.exit(1)  # this throws a system exit with code 1 to trigger CI fail
    else:
        sys.exit(0)  # clean exit for system


def check_nb_clean(nb):
    """Checks if codes cells have output that hasn't been cleared"""
    for cell in nb.cells:
        if cell.cell_type == 'code':
            if cell.outputs != []:
                return 0
    return 1


def recursive_glob(rootdir='.', pattern='*'):
    """Search recursively for files in specified directory. """
    matchingList = []
    for dirpath, dirnames, filenames in os.walk(rootdir):
        for filename in fnmatch.filter(filenames, pattern):
            matchingList.append(os.path.join(dirpath, filename))

    return matchingList


class nbNotCleanError(Exception):
    """Exception raised when nb submitted to test fails the test of
       clean output."""
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


if __name__ == '__main__':
    main()
