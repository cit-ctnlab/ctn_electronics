# TTFSSzeroCTN

### Zero representation of LIGO TTFSS boxes in CTN lab

This module is created so that all changes to the FSS boxes in CTN can be put in one place and passed onto the circuit definition when being used. This will ensure that all future uses of the zero model, defined in the [TTFSSzero package](https://git.ligo.org/40m/labutils/tree/master/TTFSSzero) are same and have all changes incorporated.

## Usage
Put TTFSSzeroCTN in your python path and in any script or notebook, do:
```
from TTFSSzeroCTN import *
```
After this, you'll get objects named NFSS and SFSS which can be used as
described in
[TTFSSzero package](https://git.ligo.org/40m/labutils/tree/master/TTFSSzero).

## Documentation
Following are some useful node/component names to remember:

|	Node name	|	Function/Parameter	|
|	---	|	---	|
|	rfbnIN	|	Input to FSS after mixer	|
|	pztnOUT	|	Output from PZT board	|
|	eomhvnOUT	|	Output from EOM HV Board	|
|   comnMIXR   |   MIXR channel port   |
|   eomnPCMon   |   PC Mon port   |
|   pztnFASTMON |   FAST Mon port |
|	comOUT1	|	Input impedance of measuring instrument connected to OUT1 at common path.<br>Default is a resistance of 50 Ohm.	|
|	comOUT2	|	Input impedance of measuring instrument connected to OUT2 at common path.<br>Default is a resistance of 50 Ohm.	|
|	pztOUT1	|	Input impedance of measuring instrument connected to OUT1 at pzt path.<br>Default is a resistance of 50 Ohm.	|
|	pztOUT2	|	Input impedance of measuring instrument connected to OUT2 at pztpath.<br>Default is a resistance of 50 Ohm.	|
|	pztCLaser	|	Capacitance of Laser PZT. <br>Added when LaserConnected(True) is used.	|
|	eomhvCEOM	|	Capacitance of EOM. <br>Added when EOMConnected(True) is used.	|

Test ports can be enabled by the corresponding test switches:

|	Test Switch Function	|	Test EXC Node	|	Test OUT1 Node	|	Test OUT2 Node	|
|	---	|	---	|	---	|	---	|
|	test1SW(circuit, state)	|	Test1ExcnIN	|	-	|	-	|
|	test2SW(circuit, state)	|	Test2ExcnIN	|	comnOUT1	|	comnOUT2	|
|	test3SW(circuit, state)	|	Test3ExcnIN	|	pztnOUT1	|	pztnOUT2	|

Additional functions:

|	Function	|	What does it do?	|	Arguments and usage	|
|	---	|	---	|	---	|
|	COMGain(circuit,G=None,Vg=None,vb=True)	|	Set common gain	|	circuit: Circuit object <br>G: Gain in dB<br>Vg:Gain setting voltage<br>vb: verbose<br>One only needs to provide one of the arguments G or Vg	|
|	FASTGain(circuit,G=None,Vg=None,vb=True)	|	Set Fast gain	|	circuit: Circuit object <br>G: Gain in dB<br>Vg:Gain setting voltage<br>vb: verbose<br>One only needs to provide one of the arguments G or Vg	|
|	PZTBoost(circuit,state)	|	Switch on or off PZT Boost. Equivalent to the side switch on TTFSS box.	|	circuit: Circuit object <br>State: 'ON' or 'OFF'	|
|	PZTSign(circuit,sign)	|	Sign selection switch same as the front panel sign selection switch in TTFSS box.	|	circuit: Circuit object <br>State: '+' or '_'	|
|	rampEngage(circuit,state)	|	Engages Ramp Switch on FSS.	|	circuit: Circuit object <br>State: 'ON' or 'OFF'	|
|	LaserConnected(circuit, connected=True)	|	Function to add or remove the laser at pzt out. This adds or removes the capacitance due to lazer pzt.	|	connected: True or False	|
|	EOMConnected(circuit, connected=True)	|	Function to add or remove the EOM at HV EOM output. This adds or removes the capacitance due to EOM.	|	connected: True or False	|
|	xxxYzz(circuit,present=False, value="0")	|	Function to add (or remove) otherwise NL component.<br> xxx: Board name 'com', 'eom' etc<br>Y: 'R' for resistor, 'C' for capacitor, 'L' for inductor<br>zz: Circuit symbol number on schematic<br> Example: comC1()	|	circuit: Circuit object <br>present: True or False<br?value: Value if present.	|

Nomenclature:
* All nodes are named as boardname + 'n' + number. Special node names which are defined for easy access are listed above.
* Boardnames are either 'rfb', 'com', 'pzt', 'eom' or 'eomhv'.
* All circuit elements are names as boardname + circuitSymbol. Ex: pztR43
* All clip testpoints are named as boardname + 'tp' + TestPointNumber + 'n'. Example 'rfbtp2n'
* Connections between different boards are through 0 $\Omega$ resistors named: board1name + 'to' + board2name

## Email
Anchal Gupta - anchal@caltech.edu
