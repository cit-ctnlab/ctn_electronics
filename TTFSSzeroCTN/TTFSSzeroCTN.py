#!/usr/bin/env python
# coding: utf-8

# ***
# # Circuit Definition (For reference)
# Following are some useful node/component names to remember:
# 
# |	Node name	|	Function/Parameter	|
# |	---	|	---	|
# |<img width=100/>| <img width=600/> |
# |	rfbnIN	|	Input to FSS after mixer	|
# |	pztnOUT	|	Output from PZT board	|
# |	eomhvnOUT	|	Output from EOM HV Board	|
# |   comnMIXR   |   MIXR channel port   |
# |   eomnPCMon   |   PC Mon port   |
# |   pztnFASTMON |   FAST Mon port |
# |	comOUT1	|	Input impedance of measuring instrument connected to OUT1 at common path.<br>Default is a resistance of 50 Ohm.	|
# |	comOUT2	|	Input impedance of measuring instrument connected to OUT2 at common path.<br>Default is a resistance of 50 Ohm.	|
# |	pztOUT1	|	Input impedance of measuring instrument connected to OUT1 at pzt path.<br>Default is a resistance of 50 Ohm.	|
# |	pztOUT2	|	Input impedance of measuring instrument connected to OUT2 at pztpath.<br>Default is a resistance of 50 Ohm.	|
# |	pztCLaser	|	Capacitance of Laser PZT. <br>Added when LaserConnected(True) is used.	|
# |	eomhvCEOM	|	Capacitance of EOM. <br>Added when EOMConnected(True) is used.	|
# 
# Test ports can be enabled by the corresponding test switches:
# 
# |	Test Switch Function	|	Test EXC Node	|	Test OUT1 Node	|	Test OUT2 Node	|
# |	---	|	---	|	---	|	---	|
# |	test1SW(circuit, state)	|	Test1ExcnIN	|	-	|	-	|
# |	test2SW(circuit, state)	|	Test2ExcnIN	|	comnOUT1	|	comnOUT2	|
# |	test3SW(circuit, state)	|	Test3ExcnIN	|	pztnOUT1	|	pztnOUT2	|
# 
# Additional functions:
# 
# |	Function	|	What does it do?	|	Arguments and usage	|
# |	---	|	---	|	---	|
# |<img width=400/>|<img width=300/>|<img width=500/>|
# |	COMGain(circuit,G=None,Vg=None,vb=True)	|	Set common gain	|	circuit: Circuit object <br>G: Gain in dB<br>Vg:Gain setting voltage<br>vb: verbose<br>One only needs to provide one of the arguments G or Vg	|
# |	FASTGain(circuit,G=None,Vg=None,vb=True)	|	Set Fast gain	|	circuit: Circuit object <br>G: Gain in dB<br>Vg:Gain setting voltage<br>vb: verbose<br>One only needs to provide one of the arguments G or Vg	|
# |	PZTBoost(circuit,state)	|	Switch on or off PZT Boost. Equivalent to the side switch on TTFSS box.	|	circuit: Circuit object <br>State: 'ON' or 'OFF'	|
# |	PZTSign(circuit,sign)	|	Sign selection switch same as the front panel sign selection switch in TTFSS box.	|	circuit: Circuit object <br>State: '+' or '_'	|
# |	rampEngage(circuit,state)	|	Engages Ramp Switch on FSS.	|	circuit: Circuit object <br>State: 'ON' or 'OFF'	|
# |	LaserConnected(connected=True)	|	Function to add or remove the laser at pzt out. This adds or removes the capacitance due to lazer pzt.	|	connected: True or False	|
# |	EOMConnected(connected=True)	|	Function to add or remove the EOM at HV EOM output. This adds or removes the capacitance due to EOM.	|	connected: True or False	|
# |	xxxYzz(circuit,present=False, value="0")	|	Function to add (or remove) otherwise NL component.<br> xxx: Board name 'com', 'eom' etc<br>Y: 'R' for resistor, 'C' for capacitor, 'L' for inductor<br>zz: Circuit symbol number on schematic<br> Example: comC1()	|	circuit: Circuit object <br>present: True or False<br?value: Value if present.	|
# 
# Nomenclature:
# * All nodes are named as boardname + 'n' + number. Special node names which are defined for easy access are listed above.
# * Boardnames are either 'rfb', 'com', 'pzt', 'eom' or 'eomhv'.
# * All circuit elements are names as boardname + circuitSymbol. Ex: pztR43
# * All clip testpoints are named as boardname + 'tp' + TestPointNumber + 'n'. Example 'rfbtp2n'
# * Connections between different boards are through 0 $\Omega$ resistors named: board1name + 'to' + board2name
# 
# ## Importing the circuit definition

# In[1]:


from TTFSS import *
import copy

# Creating two deepcopies for the North and South Boxes
NFSS = copy.deepcopy(circuit)
SFSS = copy.deepcopy(circuit)


# ***
# ## Adding changes to the circuit in CTN:
# ### These changes follow this [change log](https://nodus.ligo.caltech.edu:30889/ATFWiki/doku.php?id=main:experiments:psl:ttfss#change_log).

# ***
# ### [PSL:893 Changes](https://nodus.ligo.caltech.edu:8081/CTN/893) on North Box

# In[2]:


NFSS['comR1'].value = 49.9
NFSS['comR2'].value = 49.9
NFSS['comR7'].value = 49.9
NFSS['comR4'].value = 402.0
NFSS['comR3'].value = 56.0
NFSS['pztC35'].value = 4.7e-9


# ### [PSL:1140 Changes](https://nodus.ligo.caltech.edu:8081/CTN/1140) on South Box

# In[3]:


SFSS['comR1'].value = 47.0
SFSS['comR2'].value = 47.0
SFSS['comR7'].value = 47.0
SFSS['comR4'].value = 390.0
SFSS['comR3'].value = 100.0
SFSS['pztR29'].value = 5.1e3
SFSS['pztC35'].value = 6.8e-9


# ### [PSL:1918 Changes](https://nodus.ligo.caltech.edu:8081/CTN/1918) on North Box

# In[4]:


NFSS['comR1'].value = 470.0
NFSS['comR2'].value = 470.0


# ### [ PSL:1947 Changes](https://nodus.ligo.caltech.edu:8081/CTN/1947) on North Box

# In[5]:


NFSS['comR1'].value = 453.0
NFSS['comR2'].value = 453.0


# ### [PSL:1989 Changes](https://nodus.ligo.caltech.edu:8081/CTN/1989) on South Box

# In[6]:


SFSS['comR1'].value = 453.0
SFSS['comR2'].value = 453.0
SFSS['comR7'].value = 100.0
SFSS['comR3'].value = 124.0


# ### [PSL:2238 Changes](https://nodus.ligo.caltech.edu:8081/CTN/2238) on both North and South Boxes

# In[7]:


NFSS['rfbL3'].value = 750e-9
NFSS['rfbC12'].value = 24.4e-12
SFSS['rfbL3'].value = 750e-9
SFSS['rfbC12'].value = 22e-12


# ### [PSL:2302 Changes](https://nodus.ligo.caltech.edu:8081/CTN/2302) on North Box

# In[8]:


NFSS['eomC24'].value = 100e-9
NFSS['eomC23'].value = 33e-9
NFSS['eomR19'].value = 2e3
NFSS['pztC36'].value = 560e-12
NFSS['pztC35'].value = 6.8e-9


# ### [PSL:2303 Changes](https://nodus.ligo.caltech.edu:8081/CTN/2303) on South Box

# In[9]:


SFSS['pztR29'].value = 5.6e3


