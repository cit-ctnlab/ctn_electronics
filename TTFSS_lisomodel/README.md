This folder contains liso files and python scripts for modeling the transfer functions for the TTFSS boxes.  We will need to make modifications to loop gains and shapes to reach >600 kHz UGF so a good electronics model is essential.

Note: that all .fil files in the directory are lacking a freq command.  This is appended by the python notebook before running and importing the TF function.  This way the transfer funciton can be dynamically updated in frequency range and number of points to match datasets
