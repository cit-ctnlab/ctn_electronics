This folder contains documents related to a simple TIA with offset transducer used to interface AD590 temeperature-to-current sensors to diff output. 

The AD590 chips have a temperature to current conversion of 1uA/K with temperature referenced to 0K.  This means that there is an offset of 300 uA at room temperature, this can lead to large offsets with small actual changes.  To remedy this a voltage reference chip is connected in series with a 25.6kohm resistor into the TIA to offset to 0C. Noise on standard LT1021-7 is on order 70 nV/Hz, so a Sallen-Key LP filter might be best practice. We choose the 7V reference chip as this is as this is this is the most stable of the 5, 7, and 10V options (the other two have resistor dividers that increase V/K drift).

Current noise of the AD590 is 40 pA/rtHz with an unspecified 1/f corner.  Here the current shotnoise would be on order of 10 pA/rtHz, so device noise is in excess of the fundamental limit by a factor of four.

Selection of op amp seems less critical from a current noise perspective, although the 1/f rollup may be critical.  The main considerations are the 1/f corner point and the offset drift.

Some phase compensation might be needed to deal with potential capcitive impedance of long cables of the sensors.  Values are yet to be determined.
