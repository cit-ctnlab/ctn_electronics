
# coding: utf-8

# In[ ]:


from zero import Circuit
from zero.components import Resistor, Capacitor
import numpy as np


# In[ ]:


NPMC = Circuit()


# In[ ]:


NPMC.add_capacitor(name="C4", value="330p", node1="n0", node2="gnd")
NPMC.add_inductor(name="L1", value="3.9u", node1="n0", node2="n1")
NPMC.add_resistor(name="R10", value="49.9", node1="n1", node2="gnd")


# In[ ]:


NPMC.add_resistor(name="R4", value="220k", node1="n4", node2="gnd")
NPMC.add_capacitor(name="C9", value="2u", node1="n4", node2="gnd")
NPMC.add_resistor(name="R3", value="220k", node1="n4", node2="n2")


# In[ ]:


NPMC.add_resistor(name="R8", value="50.7", node1="nfp2test", node2="n2")


# In[ ]:


NPMC.add_resistor(name="R2", value="9.09k", node1="n2", node2="n3")
NPMC.add_resistor(name="R9", value="100", node1="n2", node2="gnd")   # Changed to 100 by awade Fe 02, 2018
NPMC.add_library_opamp(name="U2",model="lt1028", node1="n1", node2="n2", node3="n3")


# In[ ]:


NPMC.add_library_opamp(name="U10C",model="lt1125", node1="n3", node2="n3a", node3="n3b")
NPMC.add_resistor(name="R29", value="100", node1="n3a", node2="n3b")
NPMC.add_resistor(name="R30", value="100", node1="n3b", node2="nfp3test")


# In[ ]:


NPMC.add_library_opamp(name="U5A", model="ad602", node1="gnd", node2="n3", node3="n5")
NPMC.add_resistor(name="U5AinImp", value="100", node1="gnd", node2="n3")
def PMCGain(circuit,G=None,Vg=None,vb=True):
    if G is None:
        if Vg is None:
            print('Error: Provide gain in dB or gain setting voltage in V.')
            print('Nothing changed.')
            return 0
        else:
            G = 32.0*Vg + 10.0
    if G>30.0 or G<-10.0:
        print('Error: Gain can be set between -10dB to 30 dB only.')
        print('Nothing changed.')
        return 0
    circuit["U5A"].a0 = 10**(G/20) #Gain in absolute value
    if vb:
        print('PMC Gain set to '+str(np.round(G,2))+' dB.')
#Default set gain to 10 dB
PMCGain(NPMC,G=10)


# In[ ]:


NPMC.add_resistor(name="R11", value="2k", node1="n5", node2="n6")     # Changed by agupta on July 7th 2018
NPMC.add_library_opamp(name="U6", model="lt1028", node1="gnd", node2="n6", node3="n7")
NPMC.add_resistor(name="R12", value="20k", node1="n6", node2="n7")     # Changed by agupta on July 7th 2018
NPMC.add_capacitor(name="C11", value="3.98u", node1="n6", node2="n8")     # Changed by agupta on July 7th 2018
NPMC.add_resistor(name="Rnew", value="82", node1="n8", node2="n7")     # Changed by agupta on July 7th 2018


# In[ ]:


NPMC.add_resistor(name="R15", value="3.16k", node1="n7", node2="n9")     # Changed by agupta on July 7th 2018
NPMC.add_resistor(name="R23", value="3.16k", node1="n9", node2="n10")     # Changed by agupta on July 7th 2018
NPMC.add_library_opamp(name="U9", model="pa85", node1="gnd", node2="n10", node3="n11")
NPMC.add_resistor(name="R22", value="150k", node1="n10", node2="n11")
NPMC.add_resistor(name="Rout", value="39k", node1="n11", node2="nOut")     # Changed by agupta on July 12th 2018


# In[ ]:


NPMC.add_resistor(name="R14", value="121k", node1="n11", node2="n12")
NPMC.add_resistor(name="R13", value="2.49k", node1="n12", node2="gnd")


# In[ ]:


NPMC.add_library_opamp(name="U13B",model="lt1125", node1="n12", node2="n12a", node3="n12b")
NPMC.add_resistor(name="R35", value="100", node1="n12a", node2="n12b")
NPMC.add_resistor(name="R36", value="100", node1="n12b", node2="nfp4test")


# In[ ]:


def PZTConnected(circuit, connected=True):
    pztC = Capacitor(name="PZTCap", value="406.4n", node1="nOut", node2="gnd")
    if connected:
        if circuit.has_component(pztC):
            print('PZT Capacitance already present.')
        else:
            circuit.add_component(pztC)
            print('PZT Capacitance Added')
    elif circuit.has_component(pztC):
        circuit.remove_component(pztC)
        print('PZT Capacitance Removed')
    else:
        print('PZT Capacitance was not present.')

# Default: Add Lazer PZT
PZTConnected(NPMC, True)


# ## Testpoints

# In[ ]:


NPMC.add_resistor(name="TP1", value="0", node1="n3", node2="nTP1")
NPMC.add_resistor(name="TP2", value="0", node1="n5", node2="nTP2")
NPMC.add_resistor(name="TP3", value="0", node1="n12", node2="nTP3")
NPMC.add_resistor(name="TP4", value="0", node1="n7", node2="nTP4")


